var gulp = require('gulp'),
    react = require('gulp-react'),
    browserify = require('gulp-browserify'),
    rename = require('gulp-rename'),
    concat = require('gulp-concat'),
    reactify = require('reactify');

gulp.task('default', function() {
  return gulp.src('jsx/*.jsx')
      .pipe(react())
      // .pipe(browserify({ 
      //   insertGlobals: true,
      //   transform: ['reactify'],
      //   extensions: ['.jsx']
      // }))
      // .pipe(concat('bundle.js'))
      // .pipe(rename('bundle.js'))
      .pipe(gulp.dest('js'));
});

gulp.task('browserify', function() {
  return gulp.src('js/**/*.js')
    .pipe(browserify({ 
      insertGlobals: true,
      transform: ['reactify']
    }))
    .pipe(rename('bundle.js'))
    .pipe(gulp.dest('dist'));
});

gulp.task('watch', function() {
  gulp.watch('js/**/*.js', ['browserify']);
});