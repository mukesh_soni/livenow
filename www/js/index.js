/** @jsx React.DOM */

var React = require('react/addons');
var HomePage = require('./HomePage.js');
var Expenses = require('./Expenses.js');
var NotFoundPage = require('./NotFoundPage.js');
var DbHelper = require('./DbHelper.js');
var Router = require('react-router-component');
var Locations = Router.Locations;
var Location = Router.Location;
var NotFound = Router.NotFound;

var App = React.createClass({
  render: function() {
    return (
      <Locations>
        <Location path="/index.html" handler={Expenses} />
        <Location path="/expenses" handler={Expenses} />
        <NotFound handler={NotFoundPage} />
      </Locations>
    )
  }
});

// alert('hi 2 index');
var app = {
  // Application Constructor
  initialize: function() {
      this.bindEvents();
  },
  // Bind Event Listeners
  //
  // Bind any events that are required on startup. Common events are:
  // 'load', 'deviceready', 'offline', and 'online'.
  bindEvents: function() {
    document.addEventListener('deviceready', this.onDeviceReady, false);
  },
  // deviceready Event Handler
  //
  // The scope of 'this' is the event. In order to call the 'receivedEvent'
  // function, we must explicity call 'app.receivedEvent(...);'
  onDeviceReady: function() {
    DbHelper.initialize();    
    app.receivedEvent('deviceready');
  },
  // Update DOM on a Received Event
  receivedEvent: function(id) {
    React.initializeTouchEvents(true); // IMP: need to initialize touch event before rendering any component
    // alert(window.location.href);
    React.renderComponent(
      // App(),
      Expenses(),
      document.getElementById('container')
    );
  }
};

app.initialize();