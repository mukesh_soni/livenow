var DbHelper = (function() {
  var db;
  var shortName = 'livenow';
  var version = '1.0';
  var displayName = 'livenow';
  var maxSize = 65535;

  function populateDB(tx) {
    tx.executeSql('DROP TABLE IF EXISTS DEMO');
    tx.executeSql('CREATE TABLE IF NOT EXISTS DEMO (id unique, data)');
    tx.executeSql('INSERT INTO DEMO (id, data) VALUES (1, "First row")');
    tx.executeSql('INSERT INTO DEMO (id, data) VALUES (2, "Second row")');
  }

  function errorCB(err) {
    alert("Error processing SQL: "+err.code);
  }

  function successCB() {
    // alert("success!");
  }

  return {
    initialize: function() {
      db = openDatabase(shortName, version, displayName,maxSize);
      db.transaction(populateDB, errorCB, successCB);
    }
  };
}());

module.exports = DbHelper;