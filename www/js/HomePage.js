/** @jsx React.DOM */
// var React = require('../libs/react-with-addons.js');
var React = require('react/addons');
var Link = require('react-router-component').Link;

var Header = React.createClass({
  render: function() {
    return (
      <div className="topcoat-navigation-bar">
        <div className="topcoat-navigation-bar__item center full">
          <h1 className="topcoat-navigation-bar__title">It is our life!</h1>
        </div>
      </div>
    );
  }
});

var Card = React.createClass({
  goToDetails: function() {
    alert(this.props.title);
  },
  render: function() {
    return (
      <div className="card">
        <h2>
          {this.props.title}
        </h2>
        <p>
          {this.props.content}
        </p>
        <button className="topcoat-button details-button" ><Link href="/expenses">Expenses</Link></button>
        
      </div>
    );
  }
});

var CardList = React.createClass({
  render: function() {
    var cards = this.props.data.map(function(card) {
      return (<Card title={card.title} content={card.content} detailsLink={card.detailsLink}></Card>);
    });

    return (
      <div className="card-list">
        {cards}
      </div>
    );
  }
});

var HomePage = React.createClass({
  getInitialState: function() {
    return {
      data: [
        {title: "Expenses", content: "Today's expenses: Rs. 2382", detailsLink: "http://google.com"},
        {title: "Expenses", content: "Today's expenses: Rs. 2382", detailsLink: "http://google.com"},
        {title: "Expenses", content: "Today's expenses: Rs. 2382", detailsLink: "http://google.com"},
        {title: "Expenses", content: "Today's expenses: Rs. 2382", detailsLink: "http://google.com"},
        {title: "Expenses", content: "Today's expenses: Rs. 2382", detailsLink: "http://google.com"},
        {title: "Expenses", content: "Today's expenses: Rs. 2382", detailsLink: "http://google.com"},
        {title: "Card2", content: "Other content", detailsLink: "http://google.com"}
    ]}
  },
  render: function() {
    return (
      <div>
        <Header/>
        <CardList data={this.state.data} />
      </div>
    );
  }
});

// module = module || {exports: {}};
module.exports = HomePage;