/** @jsx React.DOM */
"use strict";

var React = require('react/addons');

var ExpenseHeader = React.createClass({
  render: function() {
    return (
      <div className="topcoat-navigation-bar">
        <div className="topcoat-navigation-bar__item center full">
          <h1 className="topcoat-navigation-bar__title">Expenses</h1>
        </div>
      </div>
    );
  }
});

var ExpenseTimeFilter = React.createClass({
  filterHandler: function() {
    var timeFilterValue = 'All Time';
    timeFilterValue = this.refs.timeFilter.getDOMNode().value;

    // alert(timeFilterValue);
    this.props.filterHandler(timeFilterValue);
    return false;
  },

  render: function() {
    return (
      <select onChange={this.filterHandler} ref="timeFilter">
        <option value="All Time">All Time</option>
        <option value="Monthly">Monthly</option>
        <option value="Yearly">Yearly</option>
      </select>
    );
  }
});


var ExpenseItem = React.createClass({
  render: function() {
    return (
      <li className="topcoat-list__item expense-item">
        <span className="category-label">{this.props.expense.category}</span>
        <span className="expense-amount">Rs. {this.props.expense.amount}</span>
      </li>
    );
  }
});

var ExpenseList = React.createClass({
  render: function() {
    // alert(this.props.expenses[0].amount);

    var expenses = this.props.expenses.map(function(expense) {
      return (<ExpenseItem expense={expense} />);
    });

    
    return (
      <div className="topcoat-list">
        <ul className="topcoat-list__container">
          {expenses}
        </ul>
      </div>
    );
  }
});

var Expenses = React.createClass({
  getInitialState: function() {
    return {
      data: [
        {amount: "100", category: "Movies"},
        {amount: "400", category: "Eating Out"},
        {amount: "100", category: "Movies"},
        {amount: "100", category: "Movies"},
        {amount: "100", category: "Movies"},
        {amount: "100", category: "Movies"},
        {amount: "100", category: "Movies"},
        {amount: "100", category: "Movies"}
    ]}
  },

  handleTimeFilter: function() {

  },

  render: function() {
    return (
      <div id="expenses-container">
        <ExpenseHeader />
        <div id="filter-box">
          <ExpenseTimeFilter filterHandler={this.handleTimeFilter} />
        </div>
        <ExpenseList expenses={this.state.data} />
      </div>
    );
  }
});

module.exports = Expenses;