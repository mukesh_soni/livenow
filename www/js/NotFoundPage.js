/** @jsx React.DOM */
var React = require('react/addons');

var NotFoundPage = React.createClass({
  getInitialState: function() {
    return {
      data: [
        {title: "Expenses", content: "Today's expenses: Rs. 2382", detailsLink: "http://google.com"},
        {title: "Expenses", content: "Today's expenses: Rs. 2382", detailsLink: "http://google.com"},
        {title: "Expenses", content: "Today's expenses: Rs. 2382", detailsLink: "http://google.com"},
        {title: "Expenses", content: "Today's expenses: Rs. 2382", detailsLink: "http://google.com"},
        {title: "Expenses", content: "Today's expenses: Rs. 2382", detailsLink: "http://google.com"},
        {title: "Expenses", content: "Today's expenses: Rs. 2382", detailsLink: "http://google.com"},
        {title: "Card2", content: "Other content", detailsLink: "http://google.com"}
    ]}
  },
  render: function() {
    return (
      <div>
        Nope. Nothing here.
      </div>
    );
  }
});

module.exports = NotFoundPage;